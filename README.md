# portfolio

> Weronika&#39;s Portfolio

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Requirements

- 3d tab should work the same as 2d, click to an article when hovering over a tile
  - Markdown article
- 2d and 3d thumbnail aspect ratio change (including forcing image cropping)
- Caption images on articles
- Home page should have arbitrary (but sub 10) number of article image link things
- Remove all social except artstation
- Social at footer
- Link to resume PDF or something from about page
- Keep polka she's an angel
