export default {
  mode: "spa",

  generate: {
    dir: "public",
  },

  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: "en",
    },
    title: "Weronika Kowalczyk",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { name: "theme-color", content: "#e6e6e6" },
      {
        hid: "description",
        name: "description",
        content: "The portfolio of Weronika Kowalczyk\nVideo game artist",
      },
      {
        hid: "og:image",
        property: "og:image",
        content: "/cv picture web.jpg",
      },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.jpg" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/icon?family=Material+Icons",
      },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },
  /*
   ** Global CSS
   */
  css: ["~/assets/global.css"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/google-analytics"],
  /*
   ** Google Analytics
   */
  googleAnalytics: {
    id: "UA-46217618-10",
  },
  /*
   ** Nuxt.js modules
   */
  modules: ["@nuxtjs/markdownit"],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
  },
};
